// querySelector() method returns the first element that matches a selector

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');
const Activity = document.querySelector('#activity');

// addEventListener() - method attaches an event handler to an element

// keyup - a keyboard key is realeased after being pushed

/*txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

*/
/*
	What is event target value?
		- event.target - gives you the element that triggers the event
		- event.target.value - return the element where the event occurred
*/

/*
txtFirstName.addEventListener('keyup', (event)=>{
	console.log(event.target);
	console.log(event.target.value);
})

*/


Activity.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
})
